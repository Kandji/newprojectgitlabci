<?php

namespace App\Controller;

use App\Entity\Tablette;
use App\Form\TabletteType;
use App\Repository\TabletteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tablette')]
class TabletteController extends AbstractController
{
    #[Route('/', name: 'app_tablette_index', methods: ['GET'])]
    public function index(TabletteRepository $tabletteRepository): Response
    {
        return $this->render('tablette/index.html.twig', [
            'tablettes' => $tabletteRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_tablette_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tablette = new Tablette();
        $form = $this->createForm(TabletteType::class, $tablette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tablette);
            $entityManager->flush();

            return $this->redirectToRoute('app_tablette_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tablette/new.html.twig', [
            'tablette' => $tablette,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_tablette_show', methods: ['GET'])]
    public function show(Tablette $tablette): Response
    {
        return $this->render('tablette/show.html.twig', [
            'tablette' => $tablette,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_tablette_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Tablette $tablette, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TabletteType::class, $tablette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_tablette_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tablette/edit.html.twig', [
            'tablette' => $tablette,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_tablette_delete', methods: ['POST'])]
    public function delete(Request $request, Tablette $tablette, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tablette->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tablette);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_tablette_index', [], Response::HTTP_SEE_OTHER);
    }
}
